﻿using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Xml;

namespace ArtistSearch.Api
{
    public class MusicBrainzApi
    {
        private const string ApiLocation = "http://musicbrainz.org/ws/2/";
        private const string UserAgent = @"ArtistSearch-Api/1.0.0 (kennydlawrie@gmail.com)";
        private const string AlbumTag = "cdstub";
        private const string ArtistQuery = "cdstub/?query=artist:";
        private const string AlbumQuery = "discid/";
        private const string SongTag = "title";

        public static string[] GetAlbums(string artist)
        {
            var webRequest = GetWebRequest($"{ApiLocation}{ArtistQuery}{artist}");
            using (var webResponse = (HttpWebResponse)webRequest.GetResponse())
            {
                using (var streamReader = new StreamReader(webResponse.GetResponseStream()))
                {
                    var nodes = GetXmlNodes(streamReader, AlbumTag);
                    var albumStubs = new string[nodes.Count];
                    for (var i = 0; i < nodes.Count; i++)
                    {
                        albumStubs[i] = nodes[i].Attributes[0].Value;
                    }
                    return albumStubs;
                }
            }
        }

        public static List<string> GetSongs(string[] albumStubs, bool delay)
        {
            var songs = new List<string>();
            for (var i = 0; i < albumStubs.Length; i++)
            {
                var webRequest = GetWebRequest($"{ApiLocation}{AlbumQuery}{albumStubs[i]}");
                using (var webResponse = (HttpWebResponse) webRequest.GetResponse())
                {
                    using (var streamReader = new StreamReader(webResponse.GetResponseStream()))
                    {
                        var nodes = GetXmlNodes(streamReader, SongTag);
                        Parallel.For(0, nodes.Count,
                            index => { songs.Add((nodes[index] as XmlElement)?.InnerText); });
                    }
                }

                if (delay)
                {
                    System.Threading.Thread.Sleep(1000);
                }
            }
            return songs;
        }

        private static HttpWebRequest GetWebRequest(string location)
        {
            var webRequest = (HttpWebRequest)WebRequest.Create(location);
            webRequest.Method = "GET";
            webRequest.UserAgent = UserAgent;
            return webRequest;
        }

        private static XmlNodeList GetXmlNodes(StreamReader streamReader, string tag)
        {
            var xml = streamReader.ReadToEnd();
            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xml);
            return xmlDoc.GetElementsByTagName(tag);
        }

    }
}