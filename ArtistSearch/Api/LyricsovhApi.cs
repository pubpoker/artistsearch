﻿using ArtistSearch.Models;
using System.Net.Http;
using System.Threading.Tasks;

namespace ArtistSearch.Api
{
    public class LyricsovhApi
    {
        private const string ApiLocation = "https://api.lyrics.ovh/v1/";

        public static async Task<string> GetLyrics(string artist, string song)
        {
            using (var client = new HttpClient())
            {
                LyricsJson lyrics = null;
                var response = await client.GetAsync($"{ApiLocation}{artist}/{song}");
                if (response.IsSuccessStatusCode)
                {
                    lyrics = await response.Content.ReadAsAsync<LyricsJson>();
                }

                if (lyrics != null) return lyrics.Lyrics;
            }

            return null;
        }
    }
}