﻿using System.Collections.Generic;
using ArtistSearch.Api;
using ArtistSearch.BLL;
using System.Linq;
using System.Threading.Tasks;

namespace ArtistSearch.Models
{
    public class ArtistDetails
    {
        public string Artist { get; set; }
        public int Total { get; set; }
        public string[] LyricsList { get; set; }
        public int[] WordCount { get; set; }
        public decimal AverageWords { get; set; }
        public int MaxWords { get; set; }
        public int MinWords { get; set; }
        public bool SlowMode { get; set; }

        public async Task Populate(List<string> songList)
        {
            LyricsList = new string[songList.Count];
            WordCount = new int[songList.Count];
            for (var i = 0; i < songList.Count; i++)
            {
                var lyrics = await LyricsovhApi.GetLyrics(Artist, songList[i]);
                if (lyrics == null) continue;
                LyricsList[i] = lyrics;
                WordCount[i] = Calculations.GetWordCount(LyricsList[i]);
            }
            WordCount = WordCount.Where(w => w > 0).ToArray();
            Total = WordCount.Length;
            AverageWords = Calculations.GetAverageWords(WordCount);
            MaxWords = Total != 0 ? WordCount.Max() : 0;
            MinWords = Total != 0 ? WordCount.Min() : 0;
        }
    }
}