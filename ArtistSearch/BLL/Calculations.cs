﻿using System.Linq;

namespace ArtistSearch.BLL
{
    public class Calculations
    {
        public static int GetWordCount(string lyrics)
        {
            var words = 0;
            var index = 0;
            var trimmedLyrics = string.IsNullOrEmpty(lyrics) ? string.Empty : lyrics.Trim();
            while (index < trimmedLyrics.Length)
            {
                while (index < trimmedLyrics.Length && !char.IsWhiteSpace(trimmedLyrics[index]))
                {
                    index++;
                }
                words++;
                while (index < trimmedLyrics.Length && char.IsWhiteSpace(trimmedLyrics[index]))
                {
                    index++;
                }
            }
            return words;
        }

        public static decimal GetAverageWords(int[] wordCount)
        {
            var total = 0;
            if (wordCount != null)
            {
                total += wordCount.Sum();
            }

            return total > 0 ? total / wordCount.Length : 0;
        }
    }
}