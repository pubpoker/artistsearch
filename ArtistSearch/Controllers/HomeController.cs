﻿using System.Collections.Generic;
using ArtistSearch.Api;
using ArtistSearch.Models;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ArtistSearch.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> Index(ArtistDetails artistDetils)
        {
            var albumStubs = MusicBrainzApi.GetAlbums(artistDetils.Artist);
            var songList = MusicBrainzApi.GetSongs(albumStubs, artistDetils.SlowMode);
            await artistDetils.Populate(songList);
            return View(artistDetils);
        }
    }
}