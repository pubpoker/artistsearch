﻿using ArtistSearch.BLL;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{
    [TestClass]
    public class AverageWordsTests
    {
        [TestMethod]
        public void CalculationIsCorrect()
        {
            int[] wordArray = new int[3];
            wordArray[0] = 4;
            wordArray[1] = 3;
            wordArray[2] = 8;

            var wordCount = Calculations.GetAverageWords(wordArray);

            Assert.AreEqual(5, wordCount);
        }

        [TestMethod]
        public void ZeroWordsDoesntThrowError()
        {
            int[] wordArray = new int[3];
            wordArray[0] = 0;
            wordArray[1] = 0;
            wordArray[2] = 0;

            var wordCount = Calculations.GetAverageWords(wordArray);

            Assert.AreEqual(0, wordCount);
        }

        [TestMethod]
        public void NullDoesntThrowError()
        {
            var wordCount = Calculations.GetAverageWords(null);

            Assert.AreEqual(0, wordCount);
        }
    }
}
