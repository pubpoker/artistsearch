﻿using ArtistSearch.BLL;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{
    [TestClass]
    public class WordCountTests
    {
        [TestMethod]
        public void WordCountMatchesOutcome()
        {
            var lyricsTest = @"  This is a  test for
                                7 words! 
                                ";

            var wordCount = Calculations.GetWordCount(lyricsTest);

            Assert.AreEqual(7, wordCount);
        }

        [TestMethod]
        public void ZeroWordsDoesntThrowError()
        {
            var lyricsTest = @"  
                           
                                ";

            var wordCount = Calculations.GetWordCount(lyricsTest);

            Assert.AreEqual(0, wordCount);
        }

        [TestMethod]
        public void NullDoesntThrowError()
        {
            var wordCount = Calculations.GetWordCount(null);

            Assert.AreEqual(0, wordCount);
        }
    }
}
